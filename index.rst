.. Robot documentation master file, created by
   sphinx-quickstart on Thu Feb 24 20:20:05 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MAS514's documentation!
=================================

.. image:: src/fig/uiabot.png
 

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   src/example

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
